<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="entities.Student"%>
<%@ page import="presentation.models.SearchModel"%>
<%
SearchModel model = (SearchModel) request.getAttribute("model");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Resultat de Vérification</title>
<link rel="stylesheet" type="text/css" href="webjars/bulma/0.9.3/css/bulma.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body class="has-background-grey-lighter">
	<section class="section">
		<article class="panel has-background-light">
			<%
			if (model.getResults() != null && !model.getResults().isEmpty()) {
			%>
			<p class="panel-heading">
				Resultat pour:
				<jsp:include page="model.jsp"></jsp:include>
			</p>
			<div class="panel-block">
				<div class="box my-6 table-container">
					<table class="table is-striped">
						<thead>
							<tr>
								<th>Matricule</th>
								<th>Nom</th>
								<th>Prénom</th>
								<th>Sex</th>
								<th><abbr title="Date de Naissance">DNais</abbr></th>
								<th>Faculté</th>
								<th>Département</th>
								<th>Formation</th>
								<th>Niveau</th>
								<th>Spécialité</th>
								<th><abbr title="Année de la dernière inscription">DernInsc</abbr></th>
							</tr>
						</thead>
						<tbody>
							<%
							for (Student s : model.getResults()) {
							%>
							<tr>
								<th><strong><%=s.getId()%></strong></th>
								<td><%=s.getLastName()%></td>
								<td><%=s.getFirstName()%></td>
								<td><%=s.getGender()%></td>
								<td><%=s.getBirthdate()%></td>
								<td><%=s.getFaculty()%></td>
								<td><%=s.getDepartment()%></td>
								<td><%=s.getTrainingProgram()%></td>
								<td><%=s.getLevel()%></td>
								<td><%=s.getField()%></td>
								<td><%=s.getLastInscriptionYear()%></td>
							</tr>
							<%
							}
							%>
						</tbody>
					</table>
				</div>
			</div>
		</article>
		<%
		} else {
		%>
		<p class="panel-heading">
			Aucune Resultat pour:
			<jsp:include page="model.jsp"></jsp:include>
		</p>
		<%
		}
		%>
	</section>
</body>
</html>