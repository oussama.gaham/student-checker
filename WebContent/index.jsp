<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Vérification des étudiants</title>
<link rel="stylesheet" type="text/css" href="webjars/bulma/0.9.3/css/bulma.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body class="has-background-grey-lighter">
	<section class="section">
		<article class="panel has-background-light">
			<p class="panel-heading">Vérification des étudiants</p>
			<div class="panel-block">
				<div class="container">
					<div class="columns is-centered">
						<div
							class="column m-4 is-three-quarters-mobile is-two-thirds-tablet is-half-desktop is-one-third-widescreen">
							<form class="box has-background-white-bis" action="/StudentChecker/search"
								method="get">
								<div class="field">
									<label class="label">Matricule</label>
									<div class="control">
										<input type="number" name="id" class="input" />
									</div>
								</div>
								<div class="field">
									<label class="label">Nom</label>
									<div class="control">
										<input type="search" name="lastName" class="input" />
									</div>
								</div>
								<div class="field">
									<label class="label">Prénom</label>
									<div class="control">
										<input type="search" name="firstName" class="input" />
									</div>
								</div>
								<div class="field columns is-centered pt-6 pb-2">
									<input class="button is-success" type="submit" value="Chercher" />
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</article>
	</section>
</body>
</html>