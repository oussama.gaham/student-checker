package entities;

import java.util.Date;

public class Student {

	public Student(long id, String firstName, String lastName, String gender, Date birthdate, String faculty,
			String department, String trainingProgram, String level, String field, short lastInscriptionYear) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthdate = birthdate;
		this.faculty = faculty;
		this.department = department;
		this.trainingProgram = trainingProgram;
		this.level = level;
		this.field = field;
		this.lastInscriptionYear = lastInscriptionYear;
	}

	private long id;
	private String firstName;
	private String lastName;
	private String gender;
	private Date birthdate;
	private String faculty;
	private String department;
	private String trainingProgram;
	private String level;
	private String field;
	private short lastInscriptionYear;
	
	public Student(long id, String fn, String ln) {
		this.id = id;
		firstName = fn;
		lastName = ln;
	}

	public long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getGender() {
		return gender;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public String getFaculty() {
		return faculty;
	}

	public String getDepartment() {
		return department;
	}

	public String getTrainingProgram() {
		return trainingProgram;
	}

	public String getLevel() {
		return level;
	}

	public String getField() {
		return field;
	}

	public short getLastInscriptionYear() {
		return lastInscriptionYear;
	}

}
