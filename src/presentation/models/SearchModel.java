package presentation.models;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import entities.Student;

public class SearchModel {
	
	public void retrieveParameters(HttpServletRequest req) {
		this.id = req.getParameter("id");
		this.firstName = req.getParameter("firstName");
		this.lastName = req.getParameter("lastName");
	}

	public String getId() {
		return id;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public List<Student> getResults() {
		return results;
	}

	public void setId(String id) {
		this.id = id;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setResults(List<Student> results) {
		this.results = results;
	}
	
	public SearchModel() {}
	
	private String id;
	private String firstName;
	private String lastName;
	private List<Student> results;
	
}
