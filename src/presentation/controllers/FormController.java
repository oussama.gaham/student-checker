package presentation.controllers;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.StudentDao;
import dao.StudentDaoImpl;
import presentation.models.SearchModel;

@WebServlet("/search")
public class FormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	StudentDao sDao;

	public void init(ServletConfig config) throws ServletException {
		sDao = new StudentDaoImpl();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		SearchModel model = new SearchModel();
		
		model.retrieveParameters(req);
		
		if(model.getId() != null && model.getId().isBlank()){
			model.setResults(sDao.findStudentsById(req.getParameter("id")));
		}
		else if (model.getFirstName() != null && model.getLastName() != null) {
			model.setResults(sDao.findStudentsByFullName(model.getFirstName(), model.getLastName()));
		}
		
		req.setAttribute("model", model);
		req.getRequestDispatcher("table.jsp").forward(req, res);
	}

}
