package dao;

import java.util.List;

import entities.Student;

public interface StudentDao {

	List<Student> findStudentsById(String id);
	List<Student> findStudentsByFullName(String firstName, String lastName);
	
}
