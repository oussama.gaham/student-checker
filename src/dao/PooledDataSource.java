package dao;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class PooledDataSource {
	private static BasicDataSource ds;
	static {
		ds = new BasicDataSource();

		ds.setDriverClassName("org.postgresql.Driver");
		ds.setUrl("jdbc:postgresql://172.18.0.2:5432/students");
		ds.setUsername("root");
		ds.setPassword("root");
		ds.setInitialSize(10);
		ds.setMaxTotal(10);
	}

	public static DataSource getDataSource() {
		return ds;
	}
}
