package dao;

import java.util.ArrayList;
import java.util.List;

import entities.Student;

public class MockStudentDaoImpl implements StudentDao {

	@Override
	public List<Student> findStudentsById(String id) {
		Student s1 = new Student(Long.parseLong(id), "mohamed", "mohamed");
		
		List<Student> result = new ArrayList<>();
		result.add(s1);
		
		return result;
	}

	@Override
	public List<Student> findStudentsByFullName(String firstName, String lastName) {
		Student s1 = new Student(Long.parseUnsignedLong("496573610123"), "mohamed", "mohamed");
		Student s2 = new Student(Long.parseUnsignedLong("496573610124"), "mohamed", "mohamed");
		Student s3 = new Student(Long.parseUnsignedLong("496573610125"), "mohamed", "mohamed");
		
		List<Student> result = new ArrayList<>();
		result.add(s1);
		result.add(s2);
		result.add(s3);
		
		return result;
	}

}
