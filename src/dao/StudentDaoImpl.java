package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import entities.Student;

public class StudentDaoImpl implements StudentDao {
	Connection connection;

	public StudentDaoImpl() {
		try {
			DataSource ds = PooledDataSource.getDataSource();
			connection = ds.getConnection();
			System.out.println(connection.toString());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public List<Student> findStudentsById(String id) {
		try {
			
			PreparedStatement pstmt = getStudentsByIdStatment(id);			
			
			ResultSet rs = pstmt.executeQuery();
			
			return mapResultSetToStudentsList(rs);
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	private List<Student> mapResultSetToStudentsList(ResultSet rs)
			throws SQLException {
		List<Student> results = new ArrayList<>();
		while (rs.next()) {
			Student s = new Student(
					rs.getLong("id"),
					rs.getString("firstName"),
					rs.getString("lastName"),
					rs.getString("gender"),
					rs.getDate("birthdate"),
					rs.getString("faculty"),
					rs.getString("department"),
					rs.getString("trainingProgram"),
					rs.getString("level"),
					rs.getString("field"),
					rs.getShort("lastInscriptionYear"));

			results.add(s);
		}
		return results;
	}

	private PreparedStatement getStudentsByIdStatment(String id) throws SQLException {
		PreparedStatement pstmt = connection.prepareStatement(
				"SELECT * FROM Students "+ "WHERE id = ?;");
		pstmt.setLong(1, Long.parseLong(id));
		return pstmt;
	}

	@Override
	public List<Student> findStudentsByFullName(String firstName, String lastName) {
		ResultSet rs;

		try {
			
			PreparedStatement pstmt = getStudentsByFullNameStatment(firstName, lastName);

			rs = pstmt.executeQuery();

			return mapResultSetToStudentsList(rs);
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	private PreparedStatement getStudentsByFullNameStatment(String fn, String ln) throws SQLException {
		PreparedStatement pstmt = connection
				.prepareStatement("SELECT * FROM Students " + "WHERE \"firstName\" ilike ? and \"lastName\" ilike ?;");
		pstmt.setString(1, fn);
		pstmt.setString(2, ln);
		return pstmt;
	}
}
